package io.ryp.cardano.model.cip100

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonIgnoreProperties(ignoreUnknown = true)
data class Cip100ModelGovernanceAction(
    val body: Cip100BodyGovernanceAction
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Cip100BodyGovernanceAction(
    @JsonProperty("comment")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val comment: String? = null,

    @JsonProperty("abstract")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val abstract: String? = null,

    @JsonProperty("motivation")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val motivation: String? = null,

    @JsonProperty("rationale")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val rationale: String? = null,

    @JsonProperty("title")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val title: String? = null,
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Cip100ModelDRep(
    @JsonProperty("body")
    val body: Cip100BodyDRep
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Cip100BodyDRep(
    @JsonProperty("qualifications")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val qualifications: String? = null,

    @JsonProperty("motivations")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val motivations: String? = null,

    @JsonProperty("objectives")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val objectives: String? = null,

    @JsonProperty("givenName")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val givenName: String? = null,

    @JsonProperty("bio")
    @JsonDeserialize(using = JsonLdStringDeserializer::class)
    val bio: String? = null,
)

class JsonLdStringDeserializer : JsonDeserializer<String>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): String {
        val node: JsonNode = p.codec.readTree(p)
        return when {
            node.isTextual -> node.asText()
            node.isObject && node.has("@value") -> node.get("@value").asText()
            else -> throw IllegalArgumentException("Unexpected format for string-based field")
        }
    }
}