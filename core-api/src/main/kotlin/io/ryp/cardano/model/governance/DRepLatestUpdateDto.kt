package io.ryp.cardano.model.governance

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.constraints.Pattern
import java.time.OffsetDateTime

data class DRepLatestUpdateDto @JsonCreator constructor(
    @Pattern(regexp = "^[A-Fa-f0-9]{56}$")
    @JsonProperty("drepId", required = true)
    val drepId: String,

    @Pattern(regexp = "^drep1[a-zA-Z0-9]{51}$")
    @JsonProperty("drepView", required = true)
    val drepView: String,

    @JsonProperty("currentEpoch", required = true)
    val currentEpoch: Int,

    @JsonProperty("activeUntil")
    val activeUntil: Int? = null,

    @JsonProperty("lastUpdate", required = true)
    val lastUpdate: OffsetDateTime,

    @JsonProperty("lastAnchor", required = false)
    val lastAnchor: String? = null,

    @JsonProperty("lastAnchorHash", required = false)
    val lastAnchorHash: String? = null,
)
