package io.ryp.cardano.model.governance

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import java.nio.charset.StandardCharsets

open class Cip100ServiceBase(
    private val cip100Client: WebClient,
    private val objectMapper: ObjectMapper,
    private val ipfsLink: String,
) {

    fun <T> getCip100Document(votingAnchorUrl: String, documentClass: Class<T>): Mono<T> {
        val url = when {
            votingAnchorUrl.startsWith("ipfs://") -> {
                buildIpfsGatewayUrl("ipfs://", votingAnchorUrl)
            }
            votingAnchorUrl.startsWith("https://ipfs.io/ipfs/") -> {
                buildIpfsGatewayUrl("https://ipfs.io/ipfs/", votingAnchorUrl)
            }
            else -> {
                votingAnchorUrl
            }
        }
        return getCip100DocumentHttp(url, documentClass)
    }

    private fun buildIpfsGatewayUrl(prefix: String, votingAnchorUrl: String): String {
        val ipfsCid = votingAnchorUrl.removePrefix(prefix)
        val ipfsGatewayUrl = "$ipfsLink/$ipfsCid"
        return ipfsGatewayUrl
    }

    /**
     * Fetches a CIP-100 document from the given URL via HTTP, and deals with some of the various response types, like when hosting on a CDN that only streams
     * the content (hello AWS S3) and for regular responses.
     */
    private fun <T> getCip100DocumentHttp(votingAnchorUrl: String, documentClass: Class<T>) = cip100Client.get()
        .uri(votingAnchorUrl)
        .accept(
            MediaType.APPLICATION_JSON,
            MediaType.APPLICATION_OCTET_STREAM,
            MediaType.TEXT_PLAIN,
            MediaType.valueOf("binary/octet-stream")
        )
        .exchangeToMono { response ->
            val contentType = response.headers().contentType().orElse(MediaType.APPLICATION_OCTET_STREAM)
            if (response.statusCode().isError) {
                return@exchangeToMono Mono.error(UnsupportedOperationException("Failed to fetch CIP-100 document: ${response.statusCode()}"))
            }
            when {
                contentType.includes(MediaType.APPLICATION_JSON) -> {
                    response.bodyToMono(documentClass)
                }
                contentType.includes(MediaType.TEXT_PLAIN) -> {
                    response.bodyToMono(String::class.java)
                        .map { objectMapper.readValue(it, documentClass) }
                }
                contentType.includes(MediaType.APPLICATION_OCTET_STREAM) || contentType.includes(
                    MediaType.valueOf(
                        "binary/octet-stream"
                    )
                ) -> {
                    response.bodyToMono(DataBuffer::class.java)
                        .map { buffer ->
                            val content = buffer.readableByteBuffers().asSequence()
                                .map { byteBuffer -> StandardCharsets.UTF_8.decode(byteBuffer).toString() }
                                .joinToString("")
                            objectMapper.readValue(content, documentClass)
                        }
                }
                else -> {
                    Mono.error(UnsupportedOperationException("Unsupported content type: $contentType"))
                }
            }
        }

}