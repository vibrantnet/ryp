import { Profanity, ProfanityOptions } from '@2toad/profanity';

export const createProfanityFilter = () => {
  const options = new ProfanityOptions();
  options.wholeWord = false;
  return new Profanity(options);
};
