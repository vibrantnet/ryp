import { TokenPolicy } from '@vibrantnet/core';
import { StakepoolSignup } from './StakepoolSignup';
import { DRepSignup } from './DRepSignup';

export interface ProjectData {
  name?: string;
  logo: File | null;
  url?: string;
  description?: string;
  policies: Record<string, TokenPolicy>;
  stakepool: StakepoolSignup;
  drep: DRepSignup;
}