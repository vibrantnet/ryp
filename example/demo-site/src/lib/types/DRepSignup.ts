import { DataSignature } from '@meshsdk/common';

export interface DRepSignup {
  drepId: string
  signature: DataSignature
}