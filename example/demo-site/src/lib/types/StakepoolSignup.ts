import { StakepoolVerification } from '@/lib/ryp-verification-api';

export interface StakepoolSignup {
  poolHash: string
  verification?: StakepoolVerification
}