import React, { useState } from 'react';
import { 
  Box,
  Stack,
  FormControl,
  FormLabel,
  Input,
  Button,
  Text,
  useToast
} from '@chakra-ui/react';
import { Account } from '../../lib/ryp-subscription-api';
import useTranslation from 'next-translate/useTranslation';
import Card from '../Card';
import { useApi } from '@/contexts/ApiProvider';

type ProfileSettingsProps = {
  account: Account;
  accountSettings: Record<string, string>;
};

export default function ProfileSettings({ account }: ProfileSettingsProps) {
  const { t } = useTranslation('accounts');
  const [displayName, setDisplayName] = useState(account.displayName);
  const [originalDisplayName, setOriginalDisplayName] = useState(account.displayName);
  const [isLoading, setIsLoading] = useState(false);
  const api = useApi();
  const toast = useToast();

  const handleDisplayNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDisplayName(e.target.value);
  };

  const handleSave = async () => {
    setIsLoading(true);
    try {
      await api.updateAccount(displayName);
      toast({
        title: t('profileSettings.changesSaved'),
        status: 'success',
        duration: 3000,
        isClosable: true,
      });
      setOriginalDisplayName(displayName);
    } catch (error) {
      toast({
        title: t('profileSettings.changesFailed'),
        description: t('profileSettings.changesFailedDetails'),
        status: 'error',
        duration: 3000,
        isClosable: true,
      });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box
      maxW="3xl"
      mx="auto"
      px="0"
      py={{ base: '6', md: '8', lg: '12' }}
    >
      <Stack spacing="8">
        <Card
          heading={t('profileSettings.title', { displayName: originalDisplayName })}
          description={t('profileSettings.description')}
        />
        <Box p="6">
          <FormControl id="displayName">
            <FormLabel>{t('profileSettings.displayNameLabel')}</FormLabel>
            <Input 
              value={displayName} 
              onChange={handleDisplayNameChange} 
              placeholder={t('profileSettings.displayNamePlaceholder')} 
              maxLength={100}
            />
          </FormControl>
          <Button 
            mt="4" 
            onClick={handleSave} 
            isLoading={isLoading} 
            isDisabled={!displayName || displayName === originalDisplayName}
          >
            {t('profileSettings.saveButton')}
          </Button>
          {displayName !== originalDisplayName && (
            <Text mt="2" fontSize="sm" color="gray.500">
              {t('profileSettings.unsavedChanges')}
            </Text>
          )}
        </Box>
      </Stack>
    </Box>
  );
}
