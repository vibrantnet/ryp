import { ProjectData } from "@/lib/types/ProjectData";
import { Account } from "../../lib/ryp-subscription-api";
import ProjectCategory from "@/lib/types/ProjectCategory";


export type ProjectConfigurationProps = {
  account: Account;
  type: ProjectCategory;
  formData: ProjectData;
  onSubmit: (formData: ProjectData) => Promise<void>;
};