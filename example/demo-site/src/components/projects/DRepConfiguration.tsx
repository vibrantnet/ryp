import {
    Alert,
    AlertDescription,
    AlertIcon,
    Box,
    Button,
    Container,
    Divider,
    Flex,
    FormControl,
    FormHelperText,
    FormLabel,
    Heading,
    Img,
    Input,
    Stack,
    StackDivider,
    Step,
    StepDescription,
    StepIndicator,
    StepNumber,
    Stepper,
    StepSeparator,
    StepStatus,
    StepTitle,
    Text,
    Textarea,
    useToast,
} from '@chakra-ui/react'
import { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { BrowserWallet, Wallet } from '@meshsdk/core';
import { useApi } from '@/contexts/ApiProvider';
import { ProjectConfigurationProps } from './ProjectConfigurationProps';
import { bech32ToHex } from '@/lib/cardanoutil';
import { DRepDetails } from '@/lib/ryp-verification-api';
import { MdContentCopy, MdRefresh, MdTerminal } from 'react-icons/md';


const drepIdRegex = /^drep1[a-z0-9]{51}$/i;
const signatureSample = JSON.stringify({ "COSE_Sign1_hex": "0000", "COSE_Key_hex": "0000" });

export default function DRepConfiguration({ formData, onSubmit }: ProjectConfigurationProps) {
    const [cardanoSignerCommand, setCardanoSignerCommand] = useState<string>('');
    const [drepId, setDrepId] = useState('');
    const [manualNonce, setManualNonce] = useState<string>('');
    const [challengeSignature, setChallengeSignature] = useState<string>('');
    const [walletName, setWalletName] = useState<string>('');
    const [dRepDetails, setDRepDetails] = useState<DRepDetails | null>(null);
    const [currentVerificationStep, setCurrentVerificationStep] = useState<number>(0);
    const [submitting, setSubmitting] = useState<boolean>(false);
    const drepIdRef = useRef<HTMLTextAreaElement | null>(null);
    const { t } = useTranslation('publish');
    const toast = useToast();
    const api = useApi();

    const [wallets, setWallets] = useState<Wallet[]>([]);

    useEffect(() => {
        const fetchWallets = async () => {
            const allWallets = await BrowserWallet.getAvailableWallets();
            const cip95Wallets = allWallets.filter((cw) => BrowserWallet.getSupportedExtensions(cw.id).find((ext) => +ext.cip === 95) !== undefined && cw.id !== 'nufiSnap');
            setWallets(cip95Wallets);
        };
        fetchWallets();
    }, []);

    const drepIdChanged = async (newDrepId: string) => {
        setDrepId(newDrepId);
        if (drepIdRegex.test(newDrepId)) {
            try {
                const dRepIdHash = bech32ToHex(newDrepId);
                setDRepDetails(await api.getDRepDetails(dRepIdHash));
                setCurrentVerificationStep(1);
                const nonceResponse = await api.createNonce(dRepIdHash);
                setManualNonce(nonceResponse.nonce);
                setCardanoSignerCommand(t('add.form.dreps.cardanoSignerCommand', {
                    nonce: nonceResponse.nonce,
                    dRepIdHash,
                 }));
                return;
            } catch (e) {
                console.log(e);
                toast({
                    title: t('add.form.dreps.drepNotRegistered'),
                    description: t('add.form.dreps.drepNotRegisteredDescription', { drepIdBech32: newDrepId }),
                    status: 'error',
                    isClosable: true,
                    position: 'top',
                    variant: 'solid',
                });
            }
        }
        setCurrentVerificationStep(1);
        setDRepDetails(null);
    }

    const copyCardanoSignerToClipboard = () => {
        setCurrentVerificationStep(2);
        navigator.clipboard.writeText(t('add.form.dreps.cardanoSignerCommand', {
            nonce: manualNonce,
            dRepIdHash: bech32ToHex(drepId),
            })).then(() => {
            toast({
                description: t('add.form.dreps.cardanoSignerCopiedToClipboard'),
                status: 'success',
                duration: 5000,
                isClosable: true,
            });
        });
    };

    useEffect(() => {
        if (challengeSignature.length) {
            toast.closeAll();
            try {
                const parsedSignature = JSON.parse(challengeSignature);
                const signature = parsedSignature.COSE_Sign1_hex;
                const publicKey = parsedSignature.COSE_Key_hex;
                if (signature?.length > 0 || publicKey?.length > 0) {
                    toast({
                        title: t('add.form.dreps.correctSignatureFormat'),
                        description: t('add.form.dreps.correctSignatureFormatDescription'),
                        status: 'success',
                        isClosable: true,
                        position: 'top',
                        variant: 'solid',
                    });
                    return;
                }
            } catch (e) {
                console.log(e);
            }
            toast({
                title: t('add.form.dreps.invalidSignatureFormat'),
                description: t('add.form.dreps.invalidSignatureFormatDescription'),
                status: 'error',
                isClosable: true,
                position: 'top',
                variant: 'solid',
            });
        }
    }, [challengeSignature]);

    const verifyDrep = async () => {
        setSubmitting(true);
        try {
            toast.closeAll();
            if (!dRepDetails || currentVerificationStep < 1) {
                return;
            }
            if (walletName === 'cli') {
                if (challengeSignature.length) {
                    const parsedSignature = JSON.parse(challengeSignature);
                    const signature = {
                        signature: parsedSignature.COSE_Sign1_hex,
                        key: parsedSignature.COSE_Key_hex,
                    }
                    const verified = await api.verifySignature(signature, dRepDetails.drepId);
                    if (verified) {
                        formData.drep = {
                            drepId: dRepDetails.drepId,
                            signature,
                        };
                        onSubmit(formData);
                    }
                }
            } else {
                const activeWallet = await BrowserWallet.enable(walletName, [95]);
                const pubDrepKey = await activeWallet.getPubDRepKey();
                if (!pubDrepKey) {
                    return;
                }
                const nonceResponse = await api.createNonce(dRepDetails.drepId);
                const signature = await activeWallet._walletInstance.signData(pubDrepKey.dRepIDBech32, nonceResponse.nonce);
                // const signature = await activeWallet.signData(nonceResponse.nonce, dRepDetails.drepView);
                const verified = await api.verifySignature(signature, dRepDetails.drepId);
                
                if (verified) {
                    formData.drep = {
                        drepId: dRepDetails.drepId,
                        signature,
                    };
                    onSubmit(formData);
                }
            }
        } finally {
            setSubmitting(false);
        }
    }

    const loadDrepFromWallet = async (selectedWallet: string) => {
        const activeWallet = await BrowserWallet.enable(selectedWallet, [95]);
        const drepIds = await activeWallet.getPubDRepKey();
        if (drepIds) {
            try {
                setDRepDetails(await api.getDRepDetails(drepIds.dRepIDHash));
                setDrepId(drepIds.dRepIDBech32);
                setWalletName(selectedWallet);
                setCurrentVerificationStep(2);
                return;
            } catch (e) {
                console.log(e);
            }
        }
        toast({
            title: t('add.form.dreps.drepNotRegistered'),
            description: t('add.form.dreps.drepNotRegisteredDescription', { drepIdBech32: drepIds?.dRepIDBech32 }),
            status: 'error',
            isClosable: true,
            position: 'top',
            variant: 'solid',
        });
        setDRepDetails(null);
        setCurrentVerificationStep(0);
    }

    const startCliVerification = () => {
        setWalletName('cli');
        setCurrentVerificationStep(1);
        setTimeout(() => {
            drepIdRef.current?.focus();
        }, 100);
    }

    return (<Container py={{ base: '4', md: '8' }}>
        <Stack spacing="5">
            <Stack spacing="4" direction={{ base: 'column', sm: 'row' }} justify="space-between">
                <Box>
                    <Text textStyle="lg" fontWeight="medium">
                        {t('add.form.title')}
                    </Text>
                    <Text color="fg.muted" textStyle="sm">
                        {t('add.form.cta')}
                    </Text>
                </Box>
            </Stack>
            <Divider />
            <Stack spacing="5" divider={<StackDivider />}>
                <Heading size='sm'>{t('add.form.dreps.chooseVerificationOption')}</Heading>
                <Stack spacing="3">
                    {walletName.length === 0 && wallets.map((wallet) => (
                        <Button key={wallet.name}
                            variant="secondary"
                            cursor="pointer"
                            leftIcon={<Img src={wallet.icon} alt={wallet.name} h='1.5em' w='1.5em' />}
                            onClick={() => {
                                loadDrepFromWallet(wallet.name);
                            }}
                        >
                            {wallet.name}
                        </Button>
                    ))}
                    {walletName.length === 0 && (
                        <Button key='cli'
                            variant="secondary"
                            cursor="pointer"
                            leftIcon={<MdTerminal />}
                            onClick={() => {
                                startCliVerification();
                            }}
                        >
                            {t('add.form.dreps.cliOption')}
                    </Button>
                    )}
                    {walletName.length > 0 && (<>
                        <Button
                            variant="secondary"
                            cursor="pointer"
                            leftIcon={<MdRefresh />}
                            onClick={() => {
                                setWalletName('');
                                setCurrentVerificationStep(0);
                                setDrepId('');
                                setCardanoSignerCommand('');
                                setDRepDetails(null);
                            }}
                        >
                            {t('add.form.dreps.changeVerificationOption')}
                        </Button>
                        <Alert status="info" variant="left-accent" maxW="2xl">
                            <AlertIcon />
                            <Box flex="1">
                                <AlertDescription display="block">
                                    {walletName === 'cli' && (<>{t('add.form.dreps.verifyContentsAndSubmitCli')}</>)}
                                    {walletName !== 'cli' && (<>{t('add.form.dreps.verifyContentsAndSubmitWallet')}</>)}
                                </AlertDescription>
                            </Box>
                        </Alert>
                    </>)}
                </Stack>
                {currentVerificationStep > 0 && (
                    <FormControl id="drepId" isRequired isReadOnly={walletName !== 'cli'}>
                        <Stack
                            direction={{ base: 'column', md: 'row' }}
                            spacing={{ base: '1.5', md: '8' }}
                            justify="space-between"
                        >
                            <Box w='md'>
                                <FormLabel variant="inline">{t('add.form.dreps.drepId')}</FormLabel>
                                <FormHelperText mt="0" color="fg.muted">
                                    {t('add.form.dreps.drepIdHelper')}
                                </FormHelperText>
                            </Box>
                            <Stack w="100%">
                                <Textarea rows={2} resize="none"
                                    maxW={{ md: '3xl' }}
                                    onChange={(event) => drepIdChanged(event.target.value)}
                                    value={drepId}
                                    ref={drepIdRef}
                                />
                            </Stack>
                        </Stack>
                    </FormControl>
                )}
                {currentVerificationStep > 0 && (<FormControl id="name" isReadOnly>
                    <Stack
                        direction={{ base: 'column', md: 'row' }}
                        spacing={{ base: '1.5', md: '8' }}
                        justify="space-between"
                    >
                        <Box>
                            <FormLabel variant="inline">{t('add.form.dreps.name')}</FormLabel>
                            <FormHelperText mt="0" color="fg.muted">
                                {t('add.form.automaticallyFilledOut')}
                            </FormHelperText>
                        </Box>
                        <Stack w="100%">
                            <Input maxW={{ md: '3xl' }}
                                value={dRepDetails?.displayName}
                            />
                        </Stack>
                    </Stack>
                </FormControl>)}
                {currentVerificationStep > 0 && (<FormControl id="objectives" isReadOnly>
                    <Stack
                        direction={{ base: 'column', md: 'row' }}
                        spacing={{ base: '1.5', md: '8' }}
                        justify="space-between"
                    >
                        <Box>
                            <FormLabel variant="inline">{t('add.form.dreps.objectives')}</FormLabel>
                            <FormHelperText mt="0" color="fg.muted">
                                {t('add.form.automaticallyFilledOut')}
                            </FormHelperText>
                        </Box>
                        <Stack w="100%">
                            <Box maxW={{ md: 'md' }}>{dRepDetails?.objectives}</Box>
                        </Stack>
                    </Stack>
                </FormControl>)}
                {currentVerificationStep > 0 && (<FormControl id="motivations" isReadOnly>
                    <Stack
                        direction={{ base: 'column', md: 'row' }}
                        spacing={{ base: '1.5', md: '8' }}
                        justify="space-between"
                    >
                        <Box>
                            <FormLabel variant="inline">{t('add.form.dreps.motivations')}</FormLabel>
                            <FormHelperText mt="0" color="fg.muted">
                                {t('add.form.automaticallyFilledOut')}
                            </FormHelperText>
                        </Box>
                        <Stack w="100%">
                            <Box maxW={{ md: 'md' }}>{dRepDetails?.motivations}</Box>
                        </Stack>
                    </Stack>
                </FormControl>)}
                {currentVerificationStep > 0 && (<FormControl id="qualifications" isReadOnly>
                    <Stack
                        direction={{ base: 'column', md: 'row' }}
                        spacing={{ base: '1.5', md: '8' }}
                        justify="space-between"
                    >
                        <Box>
                            <FormLabel variant="inline">{t('add.form.dreps.qualifications')}</FormLabel>
                            <FormHelperText mt="0" color="fg.muted">
                                {t('add.form.automaticallyFilledOut')}
                            </FormHelperText>
                        </Box>
                        <Stack w="100%">
                            <Box maxW={{ md: 'md' }}>{dRepDetails?.qualifications}</Box>
                        </Stack>
                    </Stack>
                </FormControl>)}
                {currentVerificationStep > 0 && walletName === 'cli' && cardanoSignerCommand.length > 0 && (<Box maxW="3xl">
                    <Text textStyle="lg" fontWeight="medium">
                        {t('add.form.dreps.drepVerification')}
                    </Text>
                    <Stepper index={currentVerificationStep} orientation='vertical' my={4} colorScheme='brand'>
                        <Step>
                            <StepIndicator>
                                <StepStatus active={<StepNumber />} complete={<StepNumber />} />
                            </StepIndicator>
                            <Box ml={4}>
                                <StepTitle>{t('add.form.dreps.step1Title')}</StepTitle>
                                <StepDescription>{t('add.form.dreps.step1Description')}</StepDescription>
                                <Stack
                                    direction={{ base: 'column' }}
                                    my={4}   
                                >
                                    <Textarea rows={6} resize="none" variant='unstyled' readOnly value={cardanoSignerCommand} />
                                    <Button alignSelf={{ sm: 'flex-end' }} onClick={copyCardanoSignerToClipboard} variant={currentVerificationStep === 0 ? 'primary' : 'outline'}><MdContentCopy /></Button>
                                </Stack>
                            </Box>
                            <StepSeparator />
                        </Step>
                        {currentVerificationStep > 1 && (<Step>
                            <StepIndicator>
                                <StepStatus active={<StepNumber />} incomplete={<StepNumber />} complete={<StepNumber />} />
                            </StepIndicator>
                            <Box ml={4}>
                                <StepTitle>{t('add.form.dreps.step2Title')}</StepTitle>
                                <StepDescription>{t('add.form.dreps.step2Description')}</StepDescription>
                                <Stack
                                    direction={{ base: 'column' }}
                                    my={4}   
                                >
                                    <FormControl id="challengeSignature" isRequired>
                                        <FormLabel variant="inline">{t('add.form.dreps.challengeSignature')}</FormLabel>
                                        <FormHelperText mt="0" color="fg.muted">
                                            {t('add.form.dreps.challengeSignatureHelper')}
                                        </FormHelperText>
                                        <Textarea
                                            rows={6}
                                            resize="none"
                                            maxW={{ md: '3xl' }}
                                            placeholder={signatureSample}
                                            onChange={(event) => setChallengeSignature(event.target.value)}
                                        />
                                    </FormControl>
                                </Stack>
                            </Box>
                            <StepSeparator />
                        </Step>)}
                    </Stepper>
                </Box>)}

                <Flex direction="row-reverse">
                    {currentVerificationStep === 2 && (<Button isLoading={submitting} onClick={verifyDrep}>{t('add.form.createDRepProject')}</Button>)}
                </Flex>
            </Stack>
        </Stack>
    </Container>)
}