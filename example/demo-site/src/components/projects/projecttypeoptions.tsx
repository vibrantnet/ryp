import { MdBusiness, MdCelebration, MdCheck, MdCurrencyExchange, MdPerson, MdWaterDrop } from 'react-icons/md';
import ProjectCategory from '@/lib/types/ProjectCategory';

const projectTypeOptions = [{
  value: ProjectCategory.NFT,
  variant: 'outline',
  icon: <MdCheck />,
  disabled: false,
}, {
  value: ProjectCategory.DeFi,
  variant: 'outline',
  icon: <MdCurrencyExchange />,
  disabled: false,
}, {
  value: ProjectCategory.DAO,
  variant: 'outline',
  icon: <MdBusiness />,
  disabled: false,
}, {
  value: ProjectCategory.Other,
  variant: 'outline',
  icon: <MdCelebration />,
  disabled: false,
}, {
  value: ProjectCategory.SPO,
  variant: 'outline',
  icon: <MdWaterDrop />,
  disabled: false,
}, {
  value: ProjectCategory.dRep,
  variant: 'outline',
  icon: <MdPerson />,
  disabled: false,
}];

export default projectTypeOptions;