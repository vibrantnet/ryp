import { Link, Text, TextProps } from '@chakra-ui/react'
import * as React from 'react'

export const Copyright = (props: TextProps) => (
  <Text fontSize="sm" {...props}>
    &copy; {new Date().getFullYear()} <Link href='https://www.vibrantsolutions.io' isExternal>Vibrant Solutions</Link>
  </Text>
)