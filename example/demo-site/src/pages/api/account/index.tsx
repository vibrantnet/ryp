import { getServerSession } from "next-auth";
import { getNextAuthOptions } from "../auth/[...nextauth]";
import { coreSubscriptionApi } from '@/lib/core-subscription-api';
import type { NextApiRequest, NextApiResponse } from 'next'
import { createProfanityFilter } from "@/lib/profanityFilter";

const profanity = createProfanityFilter();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getServerSession(
    req,
    res,
    getNextAuthOptions(req, res)
  );
  
  if (req.method === 'PATCH' && session?.userId) {
    const accountId = session.userId;
    const { displayName }: { displayName: string } = req.body;
    if (profanity.exists(displayName)) {
      res.status(400).json({ message: `Bad Request - display name includes profanity.` });
      return;
    }
    const response = await coreSubscriptionApi.updateAccountById(accountId, { displayName });
    res.status(response.status).json(response.data);
  }
}