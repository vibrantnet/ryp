import { getServerSession } from "next-auth";
import { getNextAuthOptions } from "../../../auth/[...nextauth]";
import { coreSubscriptionApi } from '@/lib/core-subscription-api';
import type { NextApiRequest, NextApiResponse } from 'next'
 
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getServerSession(
    req,
    res,
    getNextAuthOptions(req, res)
  );

  if (session?.userId) {
    const accountId = session.userId;
    if (req.method === 'DELETE') {
      const externalAccountId = +req.query.externalAccountId!;
      const response = await coreSubscriptionApi.unlinkExternalAccount(accountId, externalAccountId);
      res.status(response.status).end();
    } else if (req.method === 'PATCH' && session?.userId) {
      const { displayName }: { displayName: string } = req.body;
      const externalAccountId = +req.query.externalAccountId!;
      const response = await coreSubscriptionApi.updateLinkedExternalAccount(accountId, externalAccountId, { displayName });
      res.status(response.status).json(response.data);
    }
  } else {
    res.status(401).end();
  }
}