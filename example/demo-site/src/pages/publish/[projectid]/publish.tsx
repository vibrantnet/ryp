import PublishAnnouncement from "@/components/projects/PublishAnnouncement";
import { coreSubscriptionApi } from "@/lib/core-subscription-api";
import { verifyProjectOwnership } from "@/lib/permissions";
import { Project } from "@/lib/types/Project";
import { InferGetServerSidePropsType } from "next";
import { getServerSession } from "next-auth";
import Head from "next/head";
import { getNextAuthOptions } from "../../api/auth/[...nextauth]";

export default function Home({
  account,
  project,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  return (<>
    <Head>
      <title>RYP: Publish</title>
    </Head>
    <PublishAnnouncement account={account} project={project} />
  </>);
}

export async function getServerSideProps(context: any) {
  const session = await getServerSession(
    context.req,
    context.res,
    getNextAuthOptions(context.req, context.res)
  );
  const projectId = context.params.projectid;
  const account = (await coreSubscriptionApi.getAccountById(session?.userId ?? 0)).data;
  const accountSettings = (await coreSubscriptionApi.getSettingsForAccount(session?.userId ?? 0)).data;
  const project = (await coreSubscriptionApi.getProject(projectId)).data as Project;
  await verifyProjectOwnership(account, project);
  return {
    props: {
      session,
      account,
      accountSettings,
      project,
    },
  }
}