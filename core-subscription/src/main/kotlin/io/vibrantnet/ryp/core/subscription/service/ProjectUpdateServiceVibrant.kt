package io.vibrantnet.ryp.core.subscription.service

import io.github.oshai.kotlinlogging.KotlinLogging
import io.ryp.cardano.model.cip100.Cip100ModelDRep
import io.ryp.cardano.model.governance.DRepDetailsDto
import io.ryp.cardano.model.governance.DRepLatestUpdateDto
import io.ryp.shared.model.DRepPartialDto
import io.ryp.shared.model.ProjectPartialDto
import io.vibrantnet.ryp.core.subscription.persistence.ProjectRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.time.OffsetDateTime
import java.util.concurrent.TimeUnit

@Service
class ProjectUpdateServiceVibrant(
    private val verifyService: VerifyService,
    private val projectRepository: ProjectRepository,
    private val projectsApiService: ProjectsApiService,
    private val cip100Service: Cip100Service,
) {
    @Scheduled(fixedDelay = 15, initialDelay = 1, timeUnit = TimeUnit.MINUTES)
    fun updateDRepProjects() {
        val dRepLastUpdates = projectRepository.findDrepLastUpdates()
            .groupBy { it.projectId }
            .filterValues { it.size == 1 }
            .flatMap { it.value } // We only update projects with exactly one dRep
            .groupBy { it.drepId }
        logger.info { "Checking dReps that need to be updated based on on-chain registration changes. Found ${dRepLastUpdates.size} entries to check." }
        var updatedDReps = 0
        verifyService.getDReps()
            .doOnNext { dRep ->
                dRepLastUpdates[dRep.drepId]?.forEach { dRepLastUpdate ->
                    if (dRepLastUpdate.lastMetadataUpdate == null || dRep.lastUpdate.isAfter(dRepLastUpdate.lastMetadataUpdate)) {
                        updateDRep(dRepLastUpdate.projectId, dRep.drepId, dRep)
                        updatedDReps += 1
                    }
                }
            }
            .doAfterTerminate { logger.info { "Updated $updatedDReps dReps based on on-chain registration changes" } }
            .subscribe()
    }

    @Async
    fun updateDRep(projectId: Long, drepId: String, dRepUpdateInfo: DRepLatestUpdateDto) {
        logger.debug { "Attempting to update project $projectId and dRep $drepId with info from $dRepUpdateInfo" }
        verifyService.getDRepDetailsForDRepId(drepId)
            .flatMap { dRepDetails ->
                getDescriptionFromDetails(dRepDetails, dRepUpdateInfo)
            }.flatMap { projectPartial ->
                val projectToUpdate = projectRepository.findById(projectId)
                if (projectToUpdate.isPresent
                    && projectToUpdate.get().stakepools.isEmpty()
                    && projectToUpdate.get().policies.isEmpty()
                    && projectToUpdate.get().dreps.size == 1
                ) {
                    projectsApiService.updateProject(
                        projectId,
                        projectPartial.copy(
                            dreps = setOf(DRepPartialDto(projectToUpdate.get().dreps.first().drepId, OffsetDateTime.now()))
                        ),
                    )
                } else {
                    Mono.empty()
                }
            }
            .subscribe()
    }

    private fun getDescriptionFromDetails(
        dRepDetails: DRepDetailsDto,
        dRepLatestUpdate: DRepLatestUpdateDto
    ): Mono<ProjectPartialDto> {
        return if (
            dRepDetails.displayName.startsWith("drep1") &&
            dRepDetails.imageUrl == null &&
            dRepDetails.imageHash == null &&
            dRepDetails.motivations == null &&
            dRepDetails.qualifications == null &&
            dRepDetails.objectives == null
        ) {
            if (dRepLatestUpdate.lastAnchor == null) {
                logger.debug { "DRep ${dRepDetails.drepId} has no details from our default data source and has no anchor, cannot update project description" }
                Mono.just(ProjectPartialDto(name = getDRepDisplayName(dRepDetails)))
            } else {
                logger.debug { "DRep ${dRepDetails.drepId} has no details from our default data source, resolving anchor ourselves and getting data from ${dRepLatestUpdate.lastAnchor}" }
                cip100Service.getCip100Document(dRepLatestUpdate.lastAnchor!!, Cip100ModelDRep::class.java)
                    .map { cip100Model ->
                        val elements = listOfNotNull(
                            cip100Model.body.objectives?.takeIf { it.isNotEmpty() }?.let { "Objectives:\n$it" },
                            cip100Model.body.qualifications?.takeIf { it.isNotEmpty() }?.let { "Qualifications:\n$it" },
                            cip100Model.body.motivations?.takeIf { it.isNotEmpty() }?.let { "Motivations:\n$it" }
                        )

                        val givenName = cip100Model.body.givenName ?: getDRepDisplayName(dRepDetails)
                        ProjectPartialDto(name = givenName, description = elements.joinToString("\n\n"))
                    }
                    .onErrorResume {
                        Mono.just(ProjectPartialDto(name = getDRepDisplayName(dRepDetails)))
                    }
                    .switchIfEmpty(Mono.just(ProjectPartialDto(name = getDRepDisplayName(dRepDetails))))
            }
        } else {
            val elements = mutableListOf<String>()
            if (dRepDetails.objectives?.isNotEmpty() == true) {
                elements.add("Objectives:\n${dRepDetails.objectives}")
            }
            if (dRepDetails.qualifications?.isNotEmpty() == true) {
                elements.add("Qualifications:\n${dRepDetails.qualifications}")
            }
            if (dRepDetails.motivations?.isNotEmpty() == true) {
                elements.add("Motivations:\n${dRepDetails.motivations}")
            }
            val givenName = shortenDisplayNameIfNeeded(dRepDetails.displayName)
            Mono.just(ProjectPartialDto(name = givenName, description = elements.joinToString("\n\n")))
        }
    }

    private fun getDRepDisplayName(dRepDetails: DRepDetailsDto): String {
        return if (dRepDetails.displayName.startsWith("drep1")) {
            dRepDetails.drepView.substring(0, 11) + "…" + dRepDetails.drepView.substring(dRepDetails.drepView.length - 5)
        } else {
            dRepDetails.displayName
        }
    }

    private fun shortenDisplayNameIfNeeded(displayName: String): String {
        if (displayName.startsWith("drep1")) {
            return displayName.substring(0, 11) + "…" + displayName.substring(displayName.length - 5)
        }
        return displayName;
    }

    companion object {
        private val logger = KotlinLogging.logger {}
    }
}