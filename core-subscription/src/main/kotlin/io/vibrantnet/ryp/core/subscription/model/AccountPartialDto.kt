package io.vibrantnet.ryp.core.subscription.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.constraints.Size

data class AccountPartialDto @JsonCreator constructor(
    @JsonProperty("displayName")
    @field:Size(min = 1, max = 50) // Technical limit is 200 in the database, but for now we keep it at 50
    val displayName: String? = null,

    @JsonProperty("cardanoSettings")
    val cardanoSettings: Set<CardanoSetting>? = null,
)
