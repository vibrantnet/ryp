package io.vibrantnet.ryp.core.subscription.service

import com.fasterxml.jackson.databind.ObjectMapper
import io.ryp.cardano.model.governance.Cip100ServiceBase
import io.vibrantnet.ryp.core.subscription.CoreSubscriptionConfiguration
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

@Service
class Cip100Service(
    @Qualifier("cip100Client") cip100Client: WebClient,
    objectMapper: ObjectMapper,
    config: CoreSubscriptionConfiguration
): Cip100ServiceBase(cip100Client, objectMapper, config.ipfslink)