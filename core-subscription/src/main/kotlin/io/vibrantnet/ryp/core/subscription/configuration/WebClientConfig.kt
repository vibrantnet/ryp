package io.vibrantnet.ryp.core.subscription.configuration

import io.ryp.shared.createCoreServiceWebClientBuilder
import io.vibrantnet.ryp.core.subscription.CoreSubscriptionConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class WebClientConfig(
    private val configuration: CoreSubscriptionConfiguration,
) {
    @Bean
    fun coreVerificationClient() =
        createCoreServiceWebClientBuilder(configuration.verifyServiceUrl, configuration.security.apiKey).build()

    @Bean
    fun cip100Client(): WebClient =
        WebClient.builder()
            .exchangeStrategies(
                ExchangeStrategies.builder().codecs {
                    it.defaultCodecs().maxInMemorySize(10000000)
                }.build())
            .build()

}