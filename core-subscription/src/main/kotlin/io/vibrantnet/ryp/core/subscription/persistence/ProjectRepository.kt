package io.vibrantnet.ryp.core.subscription.persistence

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProjectRepository: CrudRepository<Project, Long> {
    fun findDistinctByRolesAccountId(accountId: Long): List<Project>

    fun findByPoliciesPolicyIdIn(policyIds: Collection<String>): List<Project>
    fun findByStakepoolsPoolHashIn(poolHashes: Collection<String>): List<Project>
    fun findByDrepsDrepIdIn(drepIds: Collection<String>): List<Project>

    @Query("SELECT p.id as projectId, d.drepId as drepId, d.lastMetadataUpdate as lastMetadataUpdate FROM Project p JOIN p.dreps d")
    fun findDrepLastUpdates(): List<DRepLastUpdateProjection>
}