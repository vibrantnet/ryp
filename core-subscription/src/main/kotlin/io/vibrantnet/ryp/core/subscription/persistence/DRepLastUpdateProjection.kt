package io.vibrantnet.ryp.core.subscription.persistence

import java.time.OffsetDateTime

interface DRepLastUpdateProjection {
    val projectId: Long
    val drepId: String
    val lastMetadataUpdate: OffsetDateTime?
}