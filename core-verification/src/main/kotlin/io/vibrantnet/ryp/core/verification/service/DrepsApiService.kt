package io.vibrantnet.ryp.core.verification.service

import io.ryp.cardano.model.governance.DRepDetailsDto
import io.ryp.cardano.model.governance.DRepLatestUpdateDto
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface DrepsApiService {

    /**
     * GET /dreps/{drepId} : Get dRep details
     *
     * @param drepId The dRep ID of a dRep (required)
     * @return The dRep details (status code 200)
     * @see DrepsApi#getDRepDetails
     */
    fun getDRepDetails(drepId: String): Mono<DRepDetailsDto>

    /**
     * GET /dreps : Get dRep hashes and last registration update block and tx info and epoch activity info
     *
     * @return The list of dRep hashes with information on when their registration was last updated and if they are still active (status code 200)
     * @see DrepsApi#getLatestDRepUpdates
     */
    fun getLatestDRepUpdates(): Flux<DRepLatestUpdateDto>
}
