package io.vibrantnet.ryp.core.verification.persistence

import io.ryp.cardano.model.governance.DRepDelegationInfoDto
import io.ryp.cardano.model.governance.DRepDetailsDto
import io.ryp.cardano.model.governance.DRepLatestUpdateDto
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface DrepDao {
    fun getDrepDetails(drepId: String): Mono<DRepDetailsDto>
    fun getLatestDRepUpdates(): Flux<DRepLatestUpdateDto>
    fun getDrepDetailsForStakeAddress(stakeAddress: String): Mono<DRepDetailsDto>
    fun getActiveDelegationWithoutAmount(drepId: String): List<DRepDelegationInfoDto>
}