package io.vibrantnet.ryp.core.verification.persistence

import io.ryp.cardano.model.governance.DRepDelegationInfoDto
import io.ryp.cardano.model.governance.DRepDetailsDto
import io.ryp.cardano.model.governance.DRepLatestUpdateDto
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.sql.ResultSet
import java.time.ZoneOffset
import java.util.*
import kotlin.NoSuchElementException

const val SQL_GET_LATEST_DREP_UPDATES = """
    WITH LatestDrepRegistration AS (SELECT r1.drep_hash_id,
                                       r1.voting_anchor_id,
                                       block.time
                                FROM drep_registration r1
                                         JOIN tx ON tx.id = r1.tx_id
                                         JOIN block ON block.id = tx.block_id
                                WHERE NOT EXISTS
                                          (SELECT TRUE
                                           FROM drep_registration r2
                                           WHERE r2.drep_hash_id = r1.drep_hash_id
                                             AND r2.tx_id > r1.tx_id)),
     LatestDrepDistr AS (SELECT hash_id,
                                MAX(epoch_no) AS latest_epoch
                         FROM drep_distr
                         GROUP BY hash_id),
     DrepDistrDetails AS (SELECT dd.hash_id,
                                 dd.epoch_no,
                                 dd.active_until
                          FROM drep_distr dd
                                   JOIN
                               LatestDrepDistr ld
                               ON
                                   dd.hash_id = ld.hash_id AND dd.epoch_no = ld.latest_epoch)
SELECT h.raw, h.view, dd.epoch_no, dd.active_until, l.time, v.url, v.data_hash
FROM LatestDrepRegistration l
         JOIN DrepDistrDetails dd ON drep_hash_id = dd.hash_id
         JOIN drep_hash h ON l.drep_hash_id = h.id
         LEFT OUTER JOIN voting_anchor v on l.voting_anchor_id=v.id
"""

const val SQL_GET_DREP_DETAILS = """
     WITH LatestDrepDistr AS (
         SELECT
             hash_id,
             MAX(epoch_no) AS latest_epoch
         FROM
             drep_distr
         GROUP BY
             hash_id
     ),
     DrepDistrDetails AS (
         SELECT
             dd.hash_id,
             dd.amount,
             dd.epoch_no,
             dd.active_until
         FROM
             drep_distr dd
                 JOIN
             LatestDrepDistr ld
             ON
                 dd.hash_id = ld.hash_id AND dd.epoch_no = ld.latest_epoch
     ),
     LatestDrepRegistration AS (SELECT * FROM drep_registration r1
        WHERE
        NOT EXISTS
            (SELECT TRUE
            FROM drep_registration r2
            WHERE r2.drep_hash_id=r1.drep_hash_id
            AND r2.tx_id>r1.tx_id)
    )
    SELECT
        h.raw,
        h.view,
        dd.epoch_no,
        dd.active_until,
        dd.amount,
        offl.given_name,
        offl.motivations,
        offl.qualifications,
        offl.objectives,
        offl.payment_address,
        offl.image_url,
        offl.image_hash
    FROM drep_hash h JOIN DrepDistrDetails dd ON h.id = dd.hash_id
         JOIN LatestDrepRegistration u ON h.id=u.drep_hash_id
         LEFT OUTER JOIN off_chain_vote_data ovd ON u.voting_anchor_id = ovd.voting_anchor_id
         LEFT OUTER JOIN off_chain_vote_drep_data offl ON ovd.id = offl.off_chain_vote_data_id
    WHERE
        h.raw = decode(?, 'hex')
"""

const val SQL_GET_DREP_DETAILS_FOR_STAKE_ADDRESS = """
    WITH LatestDelegationVote AS (
        SELECT
            addr_id,
            MAX(tx_id) AS max_tx_id
        FROM
            delegation_vote
        GROUP BY
            addr_id
    ),
    LatestDrepDistr AS (
        SELECT
            hash_id,
            MAX(epoch_no) AS latest_epoch
        FROM
            drep_distr
        GROUP BY
            hash_id
    ),
    DrepDistrDetails AS (
        SELECT
            dd.hash_id,
            dd.amount,
            dd.epoch_no,
            dd.active_until
        FROM
            drep_distr dd
                JOIN
            LatestDrepDistr ld
            ON
                dd.hash_id = ld.hash_id AND dd.epoch_no = ld.latest_epoch
    ),
    LatestDrepRegistration AS (SELECT * FROM drep_registration r1
        WHERE
        NOT EXISTS
           (SELECT TRUE
           FROM drep_registration r2
           WHERE r2.drep_hash_id=r1.drep_hash_id
           AND r2.tx_id>r1.tx_id)
    )
    SELECT
        h.raw,
        h.view,
        dd.epoch_no,
        dd.active_until,
        dd.amount,
        offl.given_name,
        offl.motivations,
        offl.qualifications,
        offl.objectives,
        offl.payment_address,
        offl.image_url,
        offl.image_hash
    FROM
        stake_address sa
            JOIN
        delegation_vote dv
        ON sa.id = dv.addr_id
            JOIN
        drep_hash h
        ON dv.drep_hash_id = h.id
            JOIN
        LatestDelegationVote ldv
        ON dv.addr_id = ldv.addr_id
            AND dv.tx_id = ldv.max_tx_id
            LEFT OUTER JOIN DrepDistrDetails dd ON h.id = dd.hash_id
            JOIN LatestDrepRegistration u ON h.id=u.drep_hash_id
            LEFT OUTER JOIN off_chain_vote_data ovd ON u.voting_anchor_id = ovd.voting_anchor_id
            LEFT OUTER JOIN off_chain_vote_drep_data offl ON ovd.id = offl.off_chain_vote_data_id
    WHERE
        sa.view = ?
"""

const val SQL_GET_ACTIVE_DELEGATION_TO_DREP_WITHOUT_AMOUNT = """
    WITH LatestDelegationVote AS (
    SELECT
        addr_id,
        MAX(tx_id) AS max_tx_id
    FROM
        delegation_vote
    GROUP BY
        addr_id
    )
    SELECT
        sa.view
    FROM
        stake_address sa
    JOIN
        delegation_vote dv
        ON sa.id = dv.addr_id
    JOIN
        drep_hash h
        ON dv.drep_hash_id = h.id
    JOIN
        LatestDelegationVote ldv
        ON dv.addr_id = ldv.addr_id
        AND dv.tx_id = ldv.max_tx_id
    WHERE
        h.raw = decode(?, 'hex')
"""

@Repository
@ConditionalOnProperty(prefix = "io.vibrantnet.ryp", name = ["type"], havingValue = "cardano-db-sync")
class DrepDaoCardanoDbSync(
    private val jdbcTemplate: JdbcTemplate,
) : DrepDao {
    @OptIn(ExperimentalStdlibApi::class)
    override fun getLatestDRepUpdates(): Flux<DRepLatestUpdateDto> {
        val utcCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        return Flux.fromIterable(jdbcTemplate.query(SQL_GET_LATEST_DREP_UPDATES) { rs, _ ->
            DRepLatestUpdateDto(
                drepId = rs.getBytes("raw").toHexString(),
                drepView = rs.getString("view"),
                currentEpoch = rs.getInt("epoch_no"),
                activeUntil = rs.getInt("active_until"),
                lastUpdate = rs.getTimestamp("time", utcCalendar).toInstant().atOffset(ZoneOffset.UTC),
                lastAnchor = rs.getString("url"),
                lastAnchorHash = rs.getBytes("data_hash")?.toHexString(),
            )
        })
    }

    override fun getDrepDetails(drepId: String): Mono<DRepDetailsDto> {
        return try {
            Mono.just(jdbcTemplate.queryForObject(SQL_GET_DREP_DETAILS.trimIndent(), { rs, _ ->
                mapDrepDetails(rs)
            }, drepId)!!)
        } catch (e: EmptyResultDataAccessException) {
            Mono.error(NoSuchElementException("No DRep details found for the given dRep ID $drepId"))
        }
    }

    override fun getDrepDetailsForStakeAddress(stakeAddress: String): Mono<DRepDetailsDto> {
        return try {
            Mono.just(jdbcTemplate.queryForObject(SQL_GET_DREP_DETAILS_FOR_STAKE_ADDRESS.trimIndent(), { rs, _ ->
                mapDrepDetails(rs)
            }, stakeAddress)!!)
        } catch (e: EmptyResultDataAccessException) {
            Mono.error(NoSuchElementException("No DRep details found for the given stake address $stakeAddress"))
        }
    }

    override fun getActiveDelegationWithoutAmount(drepId: String): List<DRepDelegationInfoDto> {
        return jdbcTemplate.query(SQL_GET_ACTIVE_DELEGATION_TO_DREP_WITHOUT_AMOUNT, { rs, _ ->
            DRepDelegationInfoDto(drepId, 1, rs.getString("view"))
        }, drepId)
    }

    @OptIn(ExperimentalStdlibApi::class)
    private fun mapDrepDetails(rs: ResultSet): DRepDetailsDto {
        var currentEpoch: Int = rs.getInt("epoch_no")
        currentEpoch = if (rs.wasNull()) 0 else currentEpoch
        var activeUntil: Int? = rs.getInt("active_until")
        activeUntil = if (rs.wasNull()) null else activeUntil
        var delegation: Long = rs.getLong("amount")
        delegation = if (rs.wasNull()) 0 else delegation
        val view = rs.getString("view")
        var displayName = rs.getString("given_name")
        if (rs.wasNull()) {
            displayName = view
        }
        return DRepDetailsDto(
            drepId = rs.getBytes("raw").toHexString(),
            drepView = rs.getString("view"),
            displayName = displayName,
            currentEpoch = currentEpoch,
            activeUntil = activeUntil,
            delegation = delegation,
            motivations = rs.getString("motivations"),
            qualifications = rs.getString("qualifications"),
            objectives = rs.getString("objectives"),
            paymentAddress = rs.getString("payment_address"),
            imageUrl = rs.getString("image_url"),
            imageHash = rs.getString("image_hash"),
        )
    }


}