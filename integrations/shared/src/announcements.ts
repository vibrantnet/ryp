import { createAnnouncementFromEvent } from "./events";
import { AnnouncementType, BasicAnnouncementDto } from "./types/announcements";
import type { CardanoNetwork } from "./types/network";

export default function augmentAnnouncementIfRequired(announcement: BasicAnnouncementDto, network: CardanoNetwork, lang: string): BasicAnnouncementDto {
  if (announcement.type !== AnnouncementType.STANDARD && announcement.type !== AnnouncementType.TEST) {
    return createAnnouncementFromEvent(announcement, network, lang);
  }
  return announcement;
  
}