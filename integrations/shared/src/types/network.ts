export enum CardanoNetwork {
  mainnet = 'mainnet',
  preprod = 'preprod',
  preview = 'preview',
  sanchonet = 'sanchonet',
}
