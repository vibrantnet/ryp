import { AnnouncementType, BasicAnnouncementDto } from "./types/announcements";
import { t } from "./translations";
import type { CardanoNetwork } from "./types/network";

const govToolsUrls: Record<CardanoNetwork, string> = {
  mainnet: 'https://gov.tools',
  preprod: 'https://preprod.gov.tools',
  preview: 'https://preview.gov.tools',
  sanchonet: 'https://sanchogov.tools',
};

const cardanoScanUrls: Record<CardanoNetwork, string> = {
  mainnet: 'https://cardanoscan.io',
  preprod: 'https://preprod.cardanoscan.io',
  preview: 'https://preview.cardanoscan.io',
  sanchonet: 'https://sanchonet.cardanoscan.io',
};

const cexplorerUrls: Record<CardanoNetwork, string> = {
  mainnet: 'https://cexplorer.io',
  preprod: 'https://preprod.cexplorer.io',
  preview: 'https://preview.cexplorer.io',
  sanchonet: 'https://sanchonet.cexplorer.io',
};

export function getProposalTitleFromAnnouncementMetadata(announcement: BasicAnnouncementDto, lang: string) {
  return announcement.metadata?.title ?? t('governance.newProposal.unknownTitle', lang, { ns: 'governance-cardano' });
}

export function getProposalTypeFromAnnouncementMetadata(announcement: BasicAnnouncementDto, lang: string) {
  return t(`governance.proposalTypes.${announcement.metadata?.proposalType}`, lang, { ns: 'governance-cardano' });
}

export function getProposalLinkFromAnnouncementMetadata(network: CardanoNetwork, announcement: BasicAnnouncementDto) {
  return `${govToolsUrls[network]}/governance_actions/${announcement.metadata?.transactionHash}#${announcement.metadata?.transactionIndex ?? 0}`;
}

export function getGovernanceVoteLinkFromAnnouncementMetadata(network: CardanoNetwork, announcement: BasicAnnouncementDto) {
  return `${cexplorerUrls[network]}/tx/${announcement.metadata?.transactionHash}/governance#data`;
}

function makeGovernanceVoteAnnouncement(announcement: BasicAnnouncementDto, network: CardanoNetwork, lang: string): BasicAnnouncementDto {
  const drepId = announcement.metadata?.drepId;
  const poolHash = announcement.metadata?.poolHash;
  const titleVar = drepId ? 'governance.newVote.titleDrep' : 'governance.newVote.titleStakepool';
  const comment = announcement.metadata?.comment;
  let voteBodyVar = 'governance.newVote.'
  if (drepId) {
    voteBodyVar += comment ? 'contentDrepComment' : 'contentDrepNoComment';
  } else if (poolHash) {
    voteBodyVar += comment ? 'contentStakepoolComment' : 'contentStakepoolNoComment';
  } else {
    // TODO: CC Vote
  }
  const link = getGovernanceVoteLinkFromAnnouncementMetadata(network, announcement);
  return {
    ...announcement,
    title: t(titleVar, lang, { ns: 'governance-cardano' }),
    content: t(voteBodyVar, lang, { ns: 'governance-cardano', comment }),
    link,
  };
}

function makeNewGovernanceProposalAnnouncement(announcement: BasicAnnouncementDto, network: CardanoNetwork, lang: string): BasicAnnouncementDto {
  const proposalTitle = getProposalTitleFromAnnouncementMetadata(announcement, lang);
  const proposalType = getProposalTypeFromAnnouncementMetadata(announcement, lang);
  const link = getProposalLinkFromAnnouncementMetadata(network, announcement);
  return {
    ...announcement,
    title: t('governance.newProposal.title', lang, { ns: 'governance-cardano' }),
    content: t('governance.newProposal.message', lang, { ns: 'governance-cardano', proposalType, proposalTitle }),
    link,
  };
}

function makeStakepoolRetirementAnnouncement(announcement: BasicAnnouncementDto, network: CardanoNetwork, lang: string): BasicAnnouncementDto {
  const poolNameOrHash = announcement.metadata?.poolName || announcement.metadata?.poolHash;
  const optionalTicker = announcement.metadata?.poolTicker ? ` (${announcement.metadata?.poolTicker})` : '';
  return {
    ...announcement,
    title: t('retirement.title', lang, { ns: 'stakepool-cardano' }),
    content: t('retirement.message', lang, { ns: 'stakepool-cardano', poolNameOrHash, optionalTicker }),
    link: `${cardanoScanUrls[network]}/transaction/${announcement.metadata?.transactionHash}`,
  };
}

export function createAnnouncementFromEvent(announcement: BasicAnnouncementDto, network: CardanoNetwork, lang: string): BasicAnnouncementDto {
  if (announcement.type === AnnouncementType.GOVERNANCE_VOTE) {
    return makeGovernanceVoteAnnouncement(announcement, network, lang);
  // eslint-disable-next-line no-else-return
  } else if (announcement.type === AnnouncementType.STAKEPOOL_RETIREMENT) {
    return makeStakepoolRetirementAnnouncement(announcement, network, lang);
  } else if (announcement.type === AnnouncementType.GOVERNANCE_ACTION_NEW_PROPOSAL) {
    return makeNewGovernanceProposalAnnouncement(announcement, network, lang);
  }
  return announcement;
}
